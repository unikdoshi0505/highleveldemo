package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

  public WebDriver driver;

 /* By emailID =By.id("email");
  By passWord= By.id("password");
  By signIn= By.xpath("By.xpath(\"//button[[@type, 'submit'] and [text()='Sign in']]\")");*/

  @FindBy(id = "email")
  public WebElement email;

  @FindBy(id="password")
  public WebElement password;

  @FindBy(xpath = "//button [text()='Sign in']")
  public WebElement signIn;

  public LoginPage(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }


}

package utility;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseClass {

  DriverClass instance = DriverClass.getInstance();
  public WebDriver driver;

  @BeforeTest
  public void setBrowser() {
    try {
      instance.setDriver("chrome");
      driver = instance.getDriver();
      driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
      driver.get("https://app.gohighlevel.com/");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @AfterTest
  public void closeBrowser() {
    driver.close();
  }

}

package BusinessFlow;

import Pages.TeamPage;
import java.util.Map;
import java.util.Random;
import org.openqa.selenium.WebDriver;

public class Teams {
  WebDriver driver;

  public Teams(WebDriver driver){
    this.driver=driver;
  }

  public static String getRandomNumberString() {
    // It will generate 6 digit random Number.
    // from 0 to 999999
    Random rnd = new Random();
    int number = rnd.nextInt(999999);

    // this will convert any number sequence into 6 character.
    return String.format("%06d", number);
  }

  public void createTeam(Map<String,String> Values){
    TeamPage teamPage= new TeamPage(driver);
    teamPage.AddTeam.click();
    teamPage.TeamInfo.click();
    teamPage.TeamName.sendKeys(Values.get("Name"));
    teamPage.AddTeam.click();
    teamPage.CalenderTeamConfiguration.click();
    teamPage.providerName.sendKeys(Values.get("Name"));
    teamPage.providerDiscreption.sendKeys(Values.get("description"));
    String CalenderURL = Values.get("URL")+getRandomNumberString();
    teamPage.providerSlug.sendKeys(CalenderURL);
    teamPage.Save.click();
  }
}

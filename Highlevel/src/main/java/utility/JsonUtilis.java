package utility;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;

public class JsonUtilis {

  public Map<String, String> getData(String FileName) throws IOException {
    String path = Paths.get(".").toAbsolutePath().normalize().toString();
    ObjectMapper mapper = new ObjectMapper();
    path = path + "\\src\\main\\resources\\"+FileName;
    System.out.println(path);
    String FileString = MyNewFileReader(path);
    JsonParser parser = new JsonParser();
    JsonObject JsonString = (JsonObject) parser.parse(FileString);
    Map<String, String> result = new ObjectMapper().readValue(String.valueOf(JsonString), new TypeReference<Map<String, String>>() {});
    return result;
  }

  public static String MyNewFileReader(String PathOfTheFile) {
    String s = "";
    try {
      File file = new File(PathOfTheFile);    //creates a new file instance
      FileReader fr = new FileReader(file);   //reads the file
      BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
      StringBuffer sb = new StringBuffer();    //constructs a string buffer with no characters
      String line;
      while ((line = br.readLine()) != null) {
        sb.append(line);      //appends line to string buffer
        sb.append("\n");     //line feed
      }
      fr.close();    //closes the stream and release the resources
      // System.out.println("Contents of File: ");
      //System.out.println(sb.toString());   //returns a string that textually represents the object
      s = sb.toString();
    } catch (IOException e) {
      e.printStackTrace();

    }
    return s;

  }

}

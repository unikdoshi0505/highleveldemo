package BusinessFlow;

import Pages.SettingsPage;
import org.openqa.selenium.WebDriver;

public class Settings {
  WebDriver driver;

  public Settings(WebDriver driver){
    this.driver=driver;
  }

  public void nevigateStaffPage(){
    SettingsPage settingsPage = new SettingsPage(driver);
    settingsPage.SetingsLink.click();
    settingsPage.MyStaffLink.click();
  }

  public void navigateTeamPage(){
    SettingsPage settingsPage = new SettingsPage(driver);
    settingsPage.SetingsLink.click();
    settingsPage.MyStaffLink.click();
    settingsPage.Teams.click();
  }
}
